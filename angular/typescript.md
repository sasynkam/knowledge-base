<!-- omit in toc -->
# Type Script

- [Project set up (via IntelliJ IDEA)](#project-set-up-via-intellij-idea)
- [Install `lite-server` to run and automatically refresh pages in the browser](#install-lite-server-to-run-and-automatically-refresh-pages-in-the-browser)
- [TypeScript "auto" compilation](#typescript-auto-compilation)

## Project set up (via IntelliJ IDEA)

See <https://www.jetbrains.com/help/idea/typescript-support.html>

- Create JavaScript project
- Enable 'Recompile on changes' (IntelliJ > File > Settings > Languages & Frameworks > Type Script > Recompile on changes)
- Generate `tsconfig.json` by right click on the project folder > New > tsconfig.js. And verify sourceMap is se to true

```json
{
  "compilerOptions": {
    "module": "commonjs",
    "target": "es5",
    "sourceMap": true
  },
  "exclude": [
    "node_modules"
  ]
}
```

- Generate `package.json` (by npm initialization in the project folder)

```console
npm init
```

- Create `index.html` file

```html
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Type Script Playground</title>
        <script src="index.js" defer></script>
    </head>

    <body>
        <h1>This is a Heading</h1>
        <p>This is a paragraph.</p>
    </body>
</html>
```

- Create type scrip `index.ts` file (will be compiled to index.js)

```ts
const message = 'Hello world!';

function sayHello(msg : string) {
    console.log(msg);
}

sayHello(message);
```

## Install `lite-server` to run and automatically refresh pages in the browser

- Installation

```console
npm install --save-dev lite-server
```

- Add `start` command to the `package.json`

```json
{
  "name": "typescript-playground",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "lite-server"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "lite-server": "^2.5.4"
  }
}
```

- run

```console
npm start
```

- Open browser console to see "Hello world! in the log

## TypeScript "auto" compilation

- Compile/Auto compile just one file

```console
tsc my_script_file.ts

tsc my_script_file.ts --watch
tsc my_script_file.ts -w
```

- Compile/Auto compile whole project

```console
# init project (needed just once)
tsc --init

tsc

tsc --watch
```

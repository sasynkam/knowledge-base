<!-- omit in toc -->
# Angular

- [Notes](#notes)
- [Install NodeJS](#install-nodejs)
- [Install/Update NPM](#installupdate-npm)
- [Install/Update TypeScript](#installupdate-typescript)
- [Install/Update Angular CLI](#installupdate-angular-cli)
- [Create workspace without project](#create-workspace-without-project)
- [Create project](#create-project)
- [Install PrimeNG](#install-primeng)
- [Install Omega layout](#install-omega-layout)
- [Run Angular development server (port 4200)](#run-angular-development-server-port-4200)
- [Setting GIT](#setting-git)

---

## Notes

- For some of commands below you need to use sudo in linux environment.
- The `-g` means it’s installed on your system globally so that the TypeScript compiler can be used in any of your projects.

## Install NodeJS

- Install by package manager (Linux) or manually: <https://github.com/nodejs/help/wiki/Installation>

```console
which node
node -v
```

## Install/Update NPM

```console
npm install -g npm
npm version
```

## Install/Update TypeScript

```console
npm install -g typescript
tsc -v
tsc -h      // help
tsc app.ts  // compiles .ts file into .js file
tsc *.ts    // same as above but using wildards
tsc *.ts --out app.js --watch   // wather
```

## Install/Update Angular CLI

```console
npm install -g @angular/cli@latest
```

```console
npm uninstall -g angular-cli @angular/cli
npm cache clean --force
npm install -g @angular/cli

ng --version
```

## Create workspace without project

```console
ng new my-workspace --create-application=false
```

## Create project

```console
ng new my-first-app
```

- Disable animations

```console
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
```

## Install PrimeNG

- Install PrimeNG <https://primefaces.org/primeng/showcase/#/setup>

```console
npm install primeng --save
npm install primeicons --save
```

- Update styles configuration in angular.json:

```console
"styles": [
  "node_modules/primeicons/primeicons.css",
  "node_modules/primeng/resources/themes/nova-light/theme.css",
  "node_modules/primeng/resources/primeng.min.css",
  //...
],
```

## Install Omega layout

- Install Omega layout <https://www.primefaces.org/omega-ng/#/documentation>

```console
npm install primeng --save
npm install primeicons --save
```

- Download mega-ng zip from PrimeStore, extract it and execute
  
```console
npm install
```

## Run Angular development server (port 4200)

```console
ng serve
```

## Setting GIT

```console
git config --global user.name "Your Name"
git config --global user.email "you@example.com"
```

```console
npm unistall -g @angular/cli@6.0.8
npm cache clean --force
npm install -g @angular/cli@6.0.8
npm install node-sass -g
ng new MY_PROJECT_NAME
cp -r from_my_old_project to_new_MY_PROJECT_NAME
```

```console
sudo ln -s /home/kamil/program/node-v13.10.1-linux-x64/bin/node /usr/bin/node
sudo ln -s /home/kamil/program/node-v13.10.1-linux-x64/bin/npm /usr/bin/npm
sudo ln -s /home/kamil/program/node-v13.10.1-linux-x64/bin/npx /usr/bin/npx
```

```console
sudo ln -sf /usr/local/lib/nodejs/node-v13.10.1-linux-x64/bin/node /usr/bin/node
sudo ln -sf /usr/local/lib/nodejs/node-v13.10.1-linux-x64/bin/npm /usr/bin/npm
sudo ln -sf /usr/local/lib/nodejs/node-v13.10.1-linux-x64/bin/npx /usr/bin/npx

ln -sf /usr/local/lib/nodejs/node-v12.16.1-linux-x64/bin/node /usr/bin/node
ln -sf /usr/local/lib/nodejs/node-v12.16.1-linux-x64/bin/npm /usr/bin/npm
ln -sf /usr/local/lib/nodejs/node-v12.16.1-linux-x64/bin/npx /usr/bin/npx
```

#VERSION=v13.10.1
#DISTRO=linux-x64
#export PATH=/home/kamil/program/node-$VERSION-$DISTRO/bin:$PATH

**********************************************************************

CREATE my-web-app2/e2e/src/app.po.ts (301 bytes)
⠇ Installing packages...npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@~2.1.2 (node_modules/chokidar/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules/watchpack/node_modules/chokidar/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.11: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules/webpack-dev-server/node_modules/chokidar/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.11: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

npm ERR! code ENOENT
npm ERR! syscall rename
npm ERR! path /home/kamil/project/angular/my-web-app2/node_modules/@angular-devkit/architect/node_modules/rxjs
npm ERR! dest /home/kamil/project/angular/my-web-app2/node_modules/@angular-devkit/architect/node_modules/.rxjs.DELETE
npm ERR! errno -2
npm ERR! enoent ENOENT: no such file or directory, rename '/home/kamil/project/angular/my-web-app2/node_modules/@angular-devkit/architect/node_modules/rxjs' -> '/home/kamil/project/angular/my-web-app2/node_modules/@angular-devkit/architect/node_modules/.rxjs.DELETE'
npm ERR! enoent This is related to npm not being able to find a file.
npm ERR! enoent 

npm ERR! A complete log of this run can be found in:
npm ERR!     /home/kamil/.npm/_logs/2020-03-08T00_21_00_521Z-debug.log
✖ Package install failed, see above.

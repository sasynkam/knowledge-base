<!-- omit in toc -->
# API Terminology, rules and best practices

- [1. References](#1-references)
- [2. Forums & Discussions](#2-forums--discussions)
- [3. Terminology](#3-terminology)
  - [3.1 Idempotency](#31-idempotency)
  - [3.2 HTTP request method](#32-http-request-method)
  - [3.3 Path & Path Parameter](#33-path--path-parameter)
  - [3.4 Query Parameter](#34-query-parameter)
  - [3.5 Body, Request Body, Response Body](#35-body-request-body-response-body)
  - [3.6 Overview](#36-overview)
- [5. Main design principles](#5-main-design-principles)
- [5. Rules](#5-rules)
  - [5.1 Use JSON](#51-use-json)
  - [5.2 Use Nouns instead of Verbs](#52-use-nouns-instead-of-verbs)
  - [5.3 Name the collections using Plural Nouns](#53-name-the-collections-using-plural-nouns)
  - [5.4 Use resource nesting to show relations or hierarchy](#54-use-resource-nesting-to-show-relations-or-hierarchy)
  - [5.5 Property names must be 'camelCase'](#55-property-names-must-be-camelcase)
  - [5.6 In URL use 'small-cases' and hyphen](#56-in-url-use-small-cases-and-hyphen)
  - [5.7 In Header use 'Pascal-Case' and hyphen](#57-in-header-use-pascal-case-and-hyphen)
  - [5.8 Skip null values in the responses](#58-skip-null-values-in-the-responses)
  - [5.9 Filtering, sorting, paging, and field selection](#59-filtering-sorting-paging-and-field-selection)
  - [5.10 Error Handling](#510-error-handling)
  - [5.11 Versioning](#511-versioning)
  - [5.12 API Documentation](#512-api-documentation)
  - [5.13 Use SSL/TLS](#513-use-ssltls)
- [6. Operations](#6-operations)
  - [6.1 GET](#61-get)
  - [6.2 POST](#62-post)
  - [6.3 PUT](#63-put)
  - [6.4 PATCH](#64-patch)
  - [6.5 DELETE](#65-delete)
  - [6.6 Overview](#66-overview)
  - [6.7 Actions that don't fit into the world of HTTP "CRUD" operations](#67-actions-that-dont-fit-into-the-world-of-http-crud-operations)
  - [6.8 Aliases for common queries](#68-aliases-for-common-queries)
- [7. Error handling](#7-error-handling)

<br/>

---

# 1. References

- Microsoft [Web API design](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design)
- Enchant [Best Practices for Designing a Pragmatic RESTful API](https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api)
- SpringDeveloper-YouTube [REST-Ful API Design](https://www.youtube.com/watch?v=oG2rotiGr90)
- Microsoft [REST API Guidelines](https://github.com/microsoft/api-guidelines/blob/vNext/Guidelines.md)
- TSU [API Guideline](https://tsutfs.tieto.com/EUCollection/TietoSmartUtility/_wiki/wikis/TietoSmartUtility.wiki?pagePath=%2FDocumentation%2FAPI%20Guideline&wikiVersion=GBwikiMaster&pageId=3231)
- Merixstudio [9 Best Practices to implement in REST API development](https://www.merixstudio.com/blog/best-practices-rest-api-development/)
- Mozilla [HTTP @ MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/HTTP)
- DZone [Path vs. Request Body Parameters](https://dzone.com/articles/rest-api-path-vs-request-body-parameters)
- Moesif [REST API Design Best Practices for Parameter and Query String Usage](https://www.moesif.com/blog/technical/api-design/REST-API-Design-Best-Practices-for-Parameters-and-Query-String-Usage/)
- Moesif [REST API Design: Filtering, Sorting, and Pagination](https://www.moesif.com/blog/technical/api-design/REST-API-Design-Filtering-Sorting-and-Pagination/)
- Spring @YouTube [How to design REST API for non-CRUD “commands” like activate and deactivate of a resource?](https://stackoverflow.com/questions/21794744/how-to-design-rest-api-for-non-crud-commands-like-activate-and-deactivate-of-a)
- ResftulAPI [REST Resource Naming Guide](https://restfulapi.net/resource-naming/)

<br/>

---

# 2. Forums & Discussions

- StackOverflow [REST API for updating information with empty or null values](https://stackoverflow.com/questions/59625400/rest-api-for-updating-informations-with-empty-or-null-values)
- StackOverflow [Use of PUT vs PATCH methods in REST API real life scenarios](https://stackoverflow.com/questions/28459418/use-of-put-vs-patch-methods-in-rest-api-real-life-scenarios/39338329#39338329)
- StackOverflow [HTTP GET with request body](https://stackoverflow.com/questions/978061/http-get-with-request-body)
- StackOverflow [What's the best practice to get user profile in RESTful API design](https://stackoverflow.com/questions/47047785/whats-the-best-practice-to-get-user-profile-in-restful-api-design)
- StackOverflow [Soft deleting](https://stackoverflow.com/questions/15839114/restful-soft-delete)

**Idea:** `GET/tickets/recently_closed`

- StackOverflow [When do I use path params vs. query params in a RESTful API?](https://stackoverflow.com/questions/30967822/when-do-i-use-path-params-vs-query-params-in-a-restful-api)

<br/>

---

# 3. Terminology

## 3.1 Idempotency

- In the context of REST APIs, when making multiple identical requests has the same effect as making a single request – then that REST API is called **idempotent**.

## 3.2 HTTP request method

- GET, POST, PUT, PATCH, DELETE and others

## 3.3 Path & Path Parameter

```url
/users/{id}
```

## 3.4 Query Parameter

```url
?sort_by=date
?sort_by=date&status=active
```

## 3.5 Body, Request Body, Response Body

```json
{
    "contractId" : 123456789,
    "startDate" : "021-03-12T21:08:21+00:00"
}
```

## 3.6 Overview

| method | path parameter | query parameter |
| ------ | -------------- | --------------- |
| `GET`  | `/users/{id}`  | `?sort-by=date` |

<br/>

---

# 5. Main design principles

- REST API is a uniform interface, which helps to decouple the client and
service implementations (and frontend and backend).

- REST API should be designed around business processes and should not be just CRUD on every entity the system has.

- REST APIs are designed around resources, which are any kind of object, data, or service that can be accessed by the client.

- A resource has an identifier, which is a URI that uniquely identifies that resource.

- Resources are often grouped together into collections.

- REST APIs use a stateless request model. HTTP requests should be independent and may occur in any order, so keeping transient state information between requests is not feasible.

- Consistent design, practices and patterns are key for development and maintenance.

- Note about [HATEOAS (Hypermedia as the Engine of Application State)](https://restfulapi.net/hateoas/) - I think it brings to much complexity on both sides - client and server. Costs are too high compared to benefits. Proper and accurate OpenAPI (formerly Swagger) documentation looks like better, more usable and more spread option.

<br/>

---

# 5. Rules

## 5.1 Use JSON

- not the yaml, xml, csv, html, plain text in the body

## 5.2 Use Nouns instead of Verbs

- and use HTTP methods to define operations on resources

| Description | Correct                    | Bad                            |
| ----------- | -------------------------- | ------------------------------ |
| get user    | GET /users/{id}            | GET /getUser/{id}              |
| create user | POST /users                | POST /createUser               |
| active user | PUT /accounts{id}/activate | PUT /account/123?activate=true |

<br/>

## 5.3 Name the collections using Plural Nouns

| Description | Correct         | Bad            |
| ----------- | --------------- | -------------- |
| get user    | GET /users/{id} | GET /user/{id} |
| create user | POST /users     | POST /user     |

<br/>

## 5.4 Use resource nesting to show relations or hierarchy

- but do not use more complex nesting than collection/item/collection
- and if a related resource can exist independently of the parent resource, it makes sense to just include an identifier for it and create separate endpoint for such resource

| Description                                    | Correct                |
| ---------------------------------------------- | ---------------------- |
| get all users                                  | /users                 |
| get specific user                              | /users/123             |
| get all orders that belongs to a specific user | /users/123/orders      |
| get specific order of a specific user          | /users/123/orders/0001 |

<br/>

## 5.5 Property names must be 'camelCase'

<br/>

## 5.6 In URL use 'small-cases' and hyphen

- not the camel case

```url
/two-words
```

## 5.7 In Header use 'Pascal-Case' and hyphen

- do not put "x" as prefix to these headers
- also verify the [standard headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Encoding) before introducing custom headers

  | key                 | value     |
  | ------------------- | --------- |
  | Accept-Encoding     | gzip      |
  | Original-Message-Id | 123456789 |

<br/>

## 5.8 Skip null values in the responses

- consider this rule:
  - pros: smaller data traffic, better human readability
  - cons: since PUT requires complete representation of resource in the request (otherwise we could end up with an undesired result), client cannot rely on GET response, so accurate and up-to-date OpenAPI (Swagger) documentation is critical point

## 5.9 Filtering, sorting, paging, and field selection

- Use paging to avoid high load and traffic
- TODO

## 5.10 Error Handling

- User standard http error codes: 200, 201, 400, ...
- Always try to provide consistent error message like

  ```json
  {
      "status" : 400,
      "message" : "No user id is provided",
      "errorCode": "21301"
  }
  ```

## 5.11 Versioning

- Use versioning once you introduce breaking/non-backward compatible changes

- `/API/v1/users`

<br/>

**Note there are other possibilities:**

- more often breaking changes = more specific versioning should be chosen

| Example                                         | Description                      |
| ----------------------------------------------- | -------------------------------- |
| `https://developer.github.com/v3/`              | major version indication only    |
| `https://us6.api.mailchimp.com/3.0/`            | major + minor version indication |
| `https://api.contoso.com/users?api-version=1.0` | major + minor version indication |
| `https://api.twilio.com/2010-04-01/`            | date based indication            |

<br/>

## 5.12 API Documentation

<br/>

## 5.13 Use SSL/TLS

<br/>

---

# 6. Operations

## 6.1 GET

**URL:**

- `GET /users/{id}`

**DESCRIPTION:**

- retrieves a representation of the resource at the specified URI
- is idempotent

**REQUEST BODY:**

- n/a (NOT APPLICABLE)
- good to return 400 Bad Request if body was in the request

**RESPONSE BODY:**

- complete representation of the requested resource

**RESPONSE STATUS CODE:**

- 200 OK
- 404 Not Found
- 400 Bad Request

## 6.2 POST

**URL:**

- `POST /users`

**DESCRIPTION:**

- a) creates a new resource at the specified URI
- b) POST request can also be used to submit data for processing to an existing resource, without any new resource being created
- is not idempotent

**REQUEST BODY:**

- complete representation of the resource

**RESPONSE HEADER:**

- a) returns URI of that newly created resource [see Spring example](https://howtodoinjava.com/spring-boot2/rest/rest-api-example/)

  | KEY      | VALUE    |
  | -------- | -------- |
  | Location | /users/1 |

- b) n/a

**RESPONSE BODY:**

- complete representation of the resource

**RESPONSE STATUS CODE:**

- a) 201 Created
- b) 200 OK (alternatively, if there is no result to return, the method can return HTTP status code 204 No Content with no response body)

## 6.3 PUT

**URL:**

- `PUT /users/{id}`

**DESCRIPTION:**

- **replaces** (updates) the **entire resource** at the specified URI
- partial resource update is not allowed with this method
- a field (it's value) can be cleared by specifying `null`
- is idempotent

**REQUEST BODY:**

- the request body must **contain a complete representation** of the resource (otherwise we could end up with an undesired result)
- good to return `400 Bad Request` when request is incomplete, so it is then explicitly mentioned (by `null`) which attribute (value) should be cleared

**RESPONSE BODY:**

- complete representation of the resource

**RESPONSE STATUS CODE:**

- 200 Ok
- 409 Conflict - if replace/updated is not not possible
- 404 Not Found
- 400 Bad Request

## 6.4 PATCH

**URL:**

- `PATCH /users/{id}`

**DESCRIPTION:**

- partial update of a resource at the specified URI
- a field (it's value) can be cleared by specifying `null`
- consider introducing this method, because there are some difficulties:
  - if the definition of the `PATCH` is strictly followed, than there must not exist any `required` fields in your rest domain model when this method is used (or dynamically disable "required" feature)
  - or you can choose liberal approach - all `required` features can not be omitted even in the `PATCH` request
- is not required to be idempotent - depends on implementation (usually it is idempotent)
- two format exists: "JSON merge patch" and more complex is "JSON patch" format

> **description of "JSON merge patch":**
>
> - patch document has the same structure as the original JSON resource
> - PATCH can be more efficient than using PUT, because the client only sends the changes, not the entire representation of the resource
> - the media type for JSON patch is `application/merge-patch+json`
>
> **description of "JSON patch":**
>
> - more complex & more flexible
> - it specifies the changes as a sequence of operations to apply. Operations include add, remove, replace, copy, and test (to validate values)
> - the media type for JSON patch is `application/json-patch+json`

<br/>

**JSON merge patch:**

**REQUEST HEADER:**

  | KEY          | VALUE                        |
  | ------------ | ---------------------------- |
  | Content-Type | application/merge-patch+json |

<br/>

**REQUEST BODY:**

- the request body **contains sub-set of changes** to the resource

**RESPONSE BODY:**

- complete representation of the resource

**RESPONSE STATUS CODE:**

- 200 Ok
- 409 Conflict
- 415 Unsupported media type
- 404 Not Found

## 6.5 DELETE

**URL:**

- `DELETE /users/{id}`

**DESCRIPTION:**

- removes the resource at the specified URI
- also "soft" delete - resource stays in the system but is not accessible any more (e.g. by some flag: inactive=true)

**REQUEST BODY:**

- no body

**RESPONSE BODY:**

- complete representation of the resource

**RESPONSE STATUS CODE:**

- 200 Ok
- 404 Not Found

<br/>

## 6.6 Overview

| Resource            | POST                              | GET                                | PUT / PATCH                                   | DELETE                           |
| ------------------- | --------------------------------- | ---------------------------------- | --------------------------------------------- | -------------------------------- |
| /customers          | Create a new customer             | Retrieve all customers             | Bulk update of customers                      | Remove all customers             |
| /customers/1        | *Error*                           | Retrieve customer 1                | Update the details of customer 1 if it exists | Remove customer 1                |
| /customers/1/orders | Create a new order for customer 1 | Retrieve all orders for customer 1 | Bulk update of orders for customer 1          | Remove all orders for customer 1 |

<br/>

## 6.7 Actions that don't fit into the world of HTTP "CRUD" operations

- There are certain cases where it is ok to use actions in a similar way to manipulating resources, eg.: PUT /users/{id}/activation
- It is tempting to use something like POST /activate_account/123 or PUT /account/123?activate=true but both of these are incorrect

**a) Resource actions:**

URL:

- `PUT /users/1/activate`

Description:

- action to given resource, so id is specified in the url
  - activation
  - close, cancel

**b) Multi-resource actions:**

URL:

- `POST customers/search`

Description:

- action within collection
  - search
  - filter

## 6.8 Aliases for common queries

URL:

- `GET /users/recently-added`

- To make the API experience more pleasant for the average consumer, consider packaging up sets of conditions into easily accessible RESTful paths.

<br>

---

# 7. Error handling

| Status Code | Message                | Detail                                                                                                                                         | Used |
| ----------- | ---------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- | ---- |
| 200         | Ok                     | Success with response body.                                                                                                                    |      |
| 201         | Created                | Success with response body.                                                                                                                    |      |
| 204         | Not Content            | Success with no response body.                                                                                                                 |      |
|             |                        |                                                                                                                                                |      |
| 400         | Bad Request            | The request URI does not match the APIs in the system, or the operation failed for unknown reasons. Invalid headers can also cause this error. |      |
| 401         | Unauthorized           | The user is not authorized to use the API.                                                                                                     | All  |
| 403         | Forbidden              | The requested operation is violating business rules or data policy constraints.                                                                |      |
| 404         | Not found              | The requested resource does not exist.                                                                                                         |      |
| 405         | Method not allowed     | The HTTP action is not allowed for the requested REST API, or it is not supported by any API.                                                  | All  |
| 406         | Not acceptable         |                                                                                                                                                |      |
| 409         | Conflict               |                                                                                                                                                |      |
| 415         | Unsupported media type | The endpoint does not support the format of the request body.                                                                                  |      |

# Welcome

Welcome on my wiki. This is my external memory :-) where you can find out tutorials, snippets and best practices.

## Clean code, code review (external links)

* [Code conventions from Oracle](http://www.oracle.com/technetwork/java/javase/documentation/codeconvtoc-136057.html)
* [Clean code Robert C. Martin](https://github.com/leonardolemie/clean-code-java)
* [Clean Code: Smells and Heuristics Robert C. Martin](https://moderatemisbehaviour.github.io/clean-code-smells-and-heuristics/)

## Environment

* [TomEE: Start & stop scripts](./environment/tomee_start_and_stop_scripts.md)
* [Apache HTTP Server as Load balancer](./environment/apache_http_server_as_load_balancer.md)

## REST API

* [API Terminology, rules and best practices](./api/01_terminology_rules_and_best_practices.md)

## Java, Spring, Maven

* [Basic setup](./java/maven_basic_project_setup.md)
* [Development and production profile in web.xml](./java/development_production_profile_in_web-xml.md)
* [Log levels (EN & CZ)](./java/log_levels.md)

## JSF, PrimeFaces, PrimeFaces Extensions, Omnifaces

* [JSF & PF - Getting started](./java/jsf-getting_started.md)

## Angular, TypeScript, JavaScript

* [Angular](./angular/angular.md)
* [TypeScript](./angular/typescript.md)

## HTML, CSS, SASS

## Databases

* [Naming conventions (CZ)](./database/naming_conventions_czech.md)
* [Java vs. SQL types](./database/java_vs_sql_types.md)

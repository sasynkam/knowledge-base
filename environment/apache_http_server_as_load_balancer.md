<!-- omit in toc -->
# Apache HTTP server (formerly known as httpd) as LoadBalancer for Apache TomEE servers

- ["Based on" internet resources](#based-on-internet-resources)
  - [Apache HTTP server - balancer, proxy](#apache-http-server---balancer-proxy)
  - [Apache & WebSocket](#apache--websocket)
  - [TLS/SSL](#tlsssl)
  - [Self-Signed Certitiface](#self-signed-certitiface)
- [Goal](#goal)
- [Known issue](#known-issue)
- [Download & Install Apache HTTP server](#download--install-apache-http-server)
  - [Paths & URLs](#paths--urls)
  - [Basic commands](#basic-commands)
- [Activate and configure Load Balancer modules](#activate-and-configure-load-balancer-modules)
  - [Required modules](#required-modules)
  - [Commands to activate/deactivate modules](#commands-to-activatedeactivate-modules)
  - [I. Apache basic configuration - balancing & balancer-manager](#i-apache-basic-configuration---balancing--balancer-manager)
  - [II. Apache websocket configuration](#ii-apache-websocket-configuration)
- [Configure Apache TomEE](#configure-apache-tomee)
- [Apache HTTP server - SSL setup (self-signed certificate)](#apache-http-server---ssl-setup-self-signed-certificate)
  - [Generate certificates](#generate-certificates)
  - [Install certificates into browsers](#install-certificates-into-browsers)
  - [III. Apache HTTPS configuration](#iii-apache-https-configuration)
  - [IV. Apache "http rewrite" configuration - rewrite all incoming http request to https](#iv-apache-http-rewrite-configuration---rewrite-all-incoming-http-request-to-https)
- [TomEE SSL - setup (reusing already created certificate)](#tomee-ssl---setup-reusing-already-created-certificate)
- [Hints](#hints)

<br/>

---

# "Based on" internet resources

## Apache HTTP server - balancer, proxy

- Apache: [Reverse proxy server also known as gateway server](https://httpd.apache.org/docs/trunk/howto/reverse_proxy.html)
- Atlassian: [How to use Apache as a Load Balancer for Confluence Data Center](https://confluence.atlassian.com/confkb/how-to-use-apache-as-a-load-balancer-for-confluence-data-center-970602623.html)
- OpenSUSE doc (also SSL) [The Apache HTTP Server](https://doc.opensuse.org/documentation/leap/reference/html/book-opensuse-reference/cha-apache2.html)
- BMC doc (also SSL) [Installing and configuring Apache Load Balancer server](https://docs.bmc.com/docs/AtriumOrchestratorPlatform/77/installing-and-configuring-apache-load-balancer-server-489036705.html)
- TecMint: [Useful Commands to Manage Apache Web Server in Linux](https://www.tecmint.com/manage-apache-web-server-in-linux/)
- ReadLearnCode [Set up Tomcat, Apache and mod_jk cluster](https://readlearncode.com/microservices/set-up-tomcat-apache-and-mod_jk-cluster)
- GeoSolutions (but AJP) [Load Balancer Setup with Apache HTTP](https://docs.geoserver.geo-solutions.it/edu/en/clustering/load_balancing/apache_http.html)
- GeoSolutions (but AJP) [Basics of clustering with Apache Tomcat](https://docs.geoserver.geo-solutions.it/edu/en/clustering/load_balancing/tomcat.html)

## Apache & WebSocket

- Stackoverflow: [WebSocket connection failed: Error during WebSocket handshake: Unexpected response code: 400](https://stackoverflow.com/questions/41381444/websocket-connection-failed-error-during-websocket-handshake-unexpected-respon)

## TLS/SSL

- FreeCodeCamp [How to get HTTPS working on your local development environment in 5 minutes](https://www.freecodecamp.org/news/how-to-get-https-working-on-your-local-development-environment-in-5-minutes-7af615770eec/)
- GeekFlare [21 OpenSSL Examples to Help You in Real-World](https://geekflare.com/openssl-commands-certificates/)
- TecAdmin [How To Enable TLS 1.3/1.2 in Apache](https://tecadmin.net/enable-tls-in-modssl-and-apache/)
- SSL com [Guide to TLS standards compliance](https://www.ssl.com/article/guide-to-tls-standards-compliance/)
- ServerFault [Using Https between Apache Loadbalancer and backends
](https://serverfault.com/questions/577616/using-https-between-apache-loadbalancer-and-backends)

## Self-Signed Certitiface

- Java Code Geeks [Self-Signed Certificate for Apache TomEE (and Tomcat)](https://www.javacodegeeks.com/2015/01/self-signed-certificate-for-apache-tomee-and-tomcat.html)
- MuleSoft [A Simple Step-By-Step Guide To Apache Tomcat SSL Configuration](https://www.mulesoft.com/tcat/tomcat-ssl)

<br/>

---

# Goal

- Apache HTTP server functioning as load balancer (via http connector)
- Two instances of Apache TomEE
- Sticky session
- Secured cookie
- Secured communication port: https
- Secured websocket port: wss
- Single Sign On

**Step 1:**
  | #   | Scheme | IP        | PORT |
  | --- | :----- | :-------- | ---: |
  | 0   | http   | localhost |   80 |
  | 1   | http   | localhost | 8081 |
  | 2   | http   | localhost | 8082 |

**Step 2:**
  | #   | Scheme | IP        | PORT |
  | --- | :----- | :-------- | ---: |
  | 0   | https  | localhost |  443 |
  | 1   | https  | localhost | 8441 |
  | 2   | https  | localhost | 8441 |

```console
                                                              -------------
                                                /-- https --> |  TomEE 1  |
                                               /              -------------
|------------|               |---------------|/
|   Client   | --- https --> | Load Balancer |
|------------|               |---------------|\
                                               \              -------------
                                                \-- https --> |  TomEE 2  |
                                                              -------------
```

<br/>

---

# Known issue

- Thanks to `org.apache.catalina.authenticator.SingleSignOn` [[link](https://tomcat.apache.org/tomcat-9.0-doc/config/valve.html)] user's identity is shared between all web applications in the same virtual host (it means in the same TomEE instance). But this session sharing does not work across multiple hosts, simply because any backend webserver isn't aware of the existence of the other.
- In normal scenario this is not problem, because Apache `mod_proxy_balancer` has one important feature: it keeps track of sessions which means that a single user is always bound to the same backend webserver.
- User is bound to different webserver (so new user authentication is required) just when original server where user is authenticated is turned off. To solve such problem, Tomcat servers must be configured into the cluster, where session replication can be enabled:

  - https://serverfault.com/questions/130127is-tomcat-shared-session-cluster-between-two-machine-possible
  - https://tomcat.apache.org/tomcat-7.0-doc/config/cluster-valve.html
  - https://docs.jboss.org/jbossclustering
  - https://www.mulesoft.com/tcat/tomcat-clustering
  - https://www.ramkitech.com/2012/10/tomcat-clustering-series-simple-load.html

<br/>

---

# Download & Install Apache HTTP server

> *Apache HTTP server version 2.4.46 released 2020-08-07*

Download Apache HTTP Server from [Apache](https://httpd.apache.org/) or
install it via linux package manager, e.g. OpenSuse:

```console
sudo zypper install apache2
```

> **Hint:** *Disable auto-start in OpenSuse:*
> 
> 1. *YaST*
> 2. *Service manager*
> 3. *Apache2 (On Boot > Manually)*

## Paths & URLs

- home `/etc/apache2/`
- installed modules 
  - see `/usr/lib64/apache2`
  - or use command: `rpm -ql apache2|grep mod`
  - or `apachectl -t -D DUMP_MODULES` or `apachectl -M`
- logs `/var/log/apache2`
- create some simple index.html page for testing purposes under `/srv/www/htdocs/`
- browser: http://localhost:80
- LB web console: http://localhost:80/balancer-manager

## Basic commands

- Help

  ```console
  sudo httpd -h
  ```

- Check version / and compiled settings

  ```console
  sudo httpd -v
  sudo httpd -V
  ```

- Check Apache Configuration Syntax Errors

  ```console
  sudo httpd -t
  ```

- Enable Apache Service

  ```console
  sudo systemctl enable apache2
  ```

- Start/Restart/Reload/Stop Apache Service

  ```console
  sudo systemctl start apache2
  sudo systemctl restart apache2
  sudo systemctl reload apache2
  sudo systemctl stop apache2
  ```

- View Apache Service Status

  ```console
  sudo systemctl status apache2
  ```

<br/>

---

# Activate and configure Load Balancer modules

## Required modules

- **headers**
- **mod_header**
- **mod_rewrite**
- **mod_proxy** - redirect connections. It allows Apache to act as a gateway to the underlying application servers
- **mod_proxy_http** - add support for proxying HTTP connections
- **mod_proxy_balancer** - add load balancing features for multiple backend servers
- **mod_proxy_wstunnel** - websocket
- **mod_slotmem_shm** - needed by mod_proxy_balancer
- **mod_lbmethod_byrequests** - parameter to specify balance by requests
- **mod_access_compat** - needed for directives like Order allow, deny, etc. rules

## Commands to activate/deactivate modules

> **Hint:** *read carefully logs `/var/log/apache2`*

- List all modules

  ```console
  sudo a2enmod -l

  output: actions alias auth_basic authn_core authn_file authz_host authz_groupfile authz_core authz_user autoindex cgi dir env expires include log_config mime negotiation setenvif ssl socache_shmcb userdir reqtimeout
  ```

- Enable module

  ```console
  sudo a2enmod MODULE
  ```

- Disable module

  ```console
  sudo a2dismod MODULE
  ```

> **Hint:** *Restart or reload Apache2 after each change*

## I. Apache basic configuration - balancing & balancer-manager

- balance http requests to backend (TomEE) servers
- and allow access to the Apache2 balancer-manager (so such requests are not routed to backend servers)

```xml
<VirtualHost *:80>

    ProxyRequests off

    Header add Set-Cookie "ROUTEID=sticky.%{BALANCER_WORKER_ROUTE}e; path=/" env=BALANCER_ROUTE_CHANGED

    <Proxy balancer://cluster_proxy>
        BalancerMember http://127.0.0.1:8081 route=route1
        BalancerMember http://127.0.0.1:8082 route=route2
        ProxySet stickysession=ROUTEID lbmethod=byrequests
    </Proxy>

    ProxyPass /balancer-manager !
    ProxyPass / balancer://cluster_proxy/

    <Location /balancer-manager>
        SetHandler balancer-manager
        Order Deny,Allow
        Allow from all
    </Location>

</VirtualHost>
```

## II. Apache websocket configuration

A **websocket** start with HTTP request like this example below. The most important part is the `Connection: Upgrade` header which let the client know to the server it wants to change to an other protocol, whose name is provided by `Upgrade: websocket` header.

```code
GET / HTTP/1.1
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Version: 13
Sec-WebSocket-Key: avkFOZvLE0gZTtEyrZPolA==
Host: localhost:8080
Sec-WebSocket-Protocol: echo-protocol
```

When a server with websocket capability receive the request above, it would answer a response like:

```code
HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: tD0l5WXr+s0lqKRayF9ABifcpzY=
Sec-WebSocket-Protocol: echo-protocol
```

```sh
<VirtualHost *:80>

    ProxyRequests off

    Header add Set-Cookie "ROUTEID=sticky.%{BALANCER_WORKER_ROUTE}e; path=/" env=BALANCER_ROUTE_CHANGED

    <Proxy balancer://cluster_proxy>
        BalancerMember http://127.0.0.1:8081 route=route1
        BalancerMember http://127.0.0.1:8082 route=route2
        ProxySet stickysession=ROUTEID lbmethod=byrequests
    </Proxy>

    <Proxy balancer://websocket_proxy>
        BalancerMember ws://127.0.0.1:8081 route=route1
        BalancerMember ws://127.0.0.1:8082 route=route2
        ProxySet stickysession=ROUTEID
    </Proxy>

    ProxyPass /balancer-manager !
    ProxyPass / balancer://cluster_proxy/

    <Location /balancer-manager>
        SetHandler balancer-manager
        Order Deny,Allow
        Allow from all
    </Location>

    # WebSocket rewrite condition and rules
    RewriteEngine On
    RewriteCond %{HTTP:Upgrade} =websocket [NC]
    RewriteRule /(.*) balancer://websocket_proxy/$1 [P,L]

</VirtualHost>
```

**Flags:**

- [NC] ... not case (case-insensitive)
- [P] ... continue to proxy directives instead of assuming it has done all the work required
- [L] ... last rule

<br/>

---

# Configure Apache TomEE

> *Apache TomEE 8.0.4; web profile*

- for setup see [tomee_start_and_stop_scripts.md](./tomee_start_and_stop_scripts.md)

# Apache HTTP server - SSL setup (self-signed certificate)

> **Hint:** *A self-signed certificate use only on a Web server that is accessed by people who know and trust you as a certificate authority. It is not recommended to use such a certificate for a public shop, for example.*

## Generate certificates

- Generate private key (`localhostPrivateKey.key`)

  ```console
  openssl genrsa -out localhostPrivateKey.key 2048
  ```

- Generate certificate signing request (`localhostCertSignReq.csr`) file
  - remember the passphrase you enter as you will need it in the future
  - to apply certificate for all subdomains use wildcard

  ```console
  openssl req -new -key localhostPrivateKey.key -out localhostCertSignReq.csr
  ```

- Generate public certificate for the Load Balancer server (`localhostPublicCert.crt`) from the generated private key and the certificate signing request file

  ```console
  openssl x509 -in localhostCertSignReq.csr -out localhostPublicCert.crt -req -signkey localhostPrivateKey.key -days 424
  ```

- See what is inside of `localhostPublicCert.crt`)

  ```console
  openssl x509 -noout -text -in localhostPublicCert.crt
  ```

- Copy public certificate and key to apache2 directories

  ```console
  sudo cp localhostPublicCert.crt /etc/apache2/ssl.crt/localhostPublicCert.crt
  sudo cp localhostPrivateKey.key /etc/apache2/ssl.key/localhostPrivateKey.key
  ```

## Install certificates into browsers

- Install `localhostPublicCert.crt` on end-user machine, so web browsers stops complaining that certificate was issued by an unknown authority.
  - Chrome: Settings -> HTTPS/SSL -> Manage Certificates … -> Authorities (tab) and then import the server.crt file.
  - Firefox: Firefox Preferences -> Advanced -> View Certificates -> Authorities (tab) and then import the server.crt file.

## III. Apache HTTPS configuration

- Verify ssl and ssl_module is active

  ```console
  sudo a2enmod -l
  ```

- If not, enable it

  ```console
  sudo a2enmod ssl
  sudo a2enmod ssl_module
  ```

- Enable ssl (case sensitive!)

  ```console
    sudo a2enflag SSL
  ```

- vhost-ssl.conf

  ```sh
  <VirtualHost _default_:443>
    #  General setup for the virtual host
    #DocumentRoot /srv/www/htdocs
    #ServerName www.example.com:443
    #ServerName localhost:443
    ServerAdmin john.black@example.com
    Loglevel debug
    ErrorLog /var/log/apache2/error_log
    TransferLog /var/log/apache2/access_log

    #   SSL Engine Switch:
    #   Enable/Disable SSL for this virtual host.
    SSLEngine on
    SSLProxyEngine on

    #SSLProxyVerify none
      SSLProxyCheckPeerName off

    #ProxyPreserveHost On

    # https://www.ssl.com/article/guide-to-tls-standards-compliance/
    SSLProtocol                 -all +TLSv1.2 +TLSv1.3
    SSLCipherSuite              ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
    SSLHonorCipherOrder         on
    SSLCompression              off

    #   OCSP Stapling:
    #   Enable/Disable OCSP for this virtual host.
    SSLUseStapling  on

    #   You can use per vhost certificates if SNI is supported.
    SSLCertificateFile /etc/apache2/ssl.crt/localhostPublicCert.crt
    SSLCertificateKeyFile /etc/apache2/ssl.key/localhostPrivateKey.key
    #SSLVerifyClient require
    #SSLVerifyDepth  1
    #SSLCACertificateFile <your CA cert file>

    #   Per-Server Logging:
    #   The home of a custom SSL log file. Use this when you want a
    #   compact non-error SSL logfile on a virtual host basis.
    CustomLog /var/log/apache2/ssl_request_log   ssl_combined

      ProxyRequests off

      Header add Set-Cookie "ROUTEID=sticky.%{BALANCER_WORKER_ROUTE}e; path=/" env=BALANCER_ROUTE_CHANGED

      <Proxy balancer://cluster_proxy>
          BalancerMember https://localhost:8441 route=route1
          BalancerMember https://localhost:8442 route=route2
          ProxySet stickysession=ROUTEID lbmethod=byrequests
      </Proxy>

      <Proxy balancer://websocket_proxy>
          BalancerMember wss://localhost:8441 route=route1
          BalancerMember wss://localhost:8442 route=route2
          ProxySet stickysession=ROUTEID
      </Proxy>

      ProxyPass /balancer-manager !
      ProxyPass / balancer://cluster_proxy/
      ProxyPassReverse / balancer://cluster_proxy/

      <Location /balancer-manager>
          SetHandler balancer-manager
          Order Deny,Allow
          #Require ip 192.168.56.0/24 127.0.0.1/24
          # Allow from 127.0.0.1 ::1
          Allow from all
      </Location>

      # WebSocket rewrite condition and rules
      RewriteEngine On
      RewriteCond %{HTTP:Upgrade} =websocket [NC]
      RewriteRule /(.*) balancer://websocket_proxy/$1 [P,L]

  </VirtualHost>  
  ```

## IV. Apache "http rewrite" configuration - rewrite all incoming http request to https

- vhost.conf

  ```sh
  <VirtualHost *:80>

    RewriteEngine On
    RewriteCond %{HTTPS} !=on
    RewriteRule ^/?(.*) https://%{SERVER_NAME}/$1 [R,L]

  </VirtualHost>
  ```

<br/>

---

# TomEE SSL - setup (reusing already created certificate)

Apache HTTP server uses x509 pem/crt files which is very different than a Apache Tomcat/TomEE requires. To use certificate inside Tomcat/TomEE we need to use a keystore. This keystore is generated using keytool command. To use this tool, the certificate should be of PKCS12 format.

- Convert certificate from PEM format to PKCS12 format (`localhostPublicCert.crt.p12`):

  ```console
  openssl pkcs12 -export -in localhostPublicCert.crt -inkey localhostPrivateKey.key -out localhostPublicCert.crt.p12 -name test_server -caname root_ca
  ```

- Create keystore (`keystore.jks`)

  ```console
  keytool -importkeystore -destkeystore keystore.jks -srckeystore localhostPublicCert.crt.p12 -srcstoretype PKCS12 -srcalias test_server -destalias test_server
  ```

- Uncomment and configure https TomEE connector (`conf/server.xml`)
  - for setup see [tomee_start_and_stop_scripts.md](./tomee_start_and_stop_scripts.md)

  ```xml
  <Connector port="${tomee.port.https}"
        protocol="org.apache.coyote.http11.Http11NioProtocol"
        maxThreads="150"
        SSLEnabled="true">
    <SSLHostConfig>
      <Certificate certificateKeystoreFile="${tomee.cert.keystorefile}"
            certificateKeystorePassword="${tomee.cert.keystorepassword}"
            certificateKeyAlias="${tomee.cert.keystorealias}"
            type="RSA"
            xpoweredBy="false"
            server="Apache TomEE" />
    </SSLHostConfig>
  </Connector>
  ```

- Comment out http TomEE connector (`conf/server.xml`) since all http request are routed by Apache HTTP server to the https (see above).

> **Hint:** To be able to use TomEE https connector in linux, you need to install `libtcnative` package.

<br/>

---

# Hints

- Why to use HTTP protocol instead of AJP: https://serverfault.com/questions/878629/apache-cluster-tomcat-websocket

- How to Monitor Apache Web Server Load and Page Statistics: https://www.tecmint.com/monitor-apache-web-server-load-and-page-statistics
  
- How to import certificate to Chromium: https://chromium.googlesource.com/chromium/src/+/master/docs/linux/cert_management.md
- Firefox https://kamarada.github.io/en/2018/10/30/how-to-install-website-certificates-on-linux/#.X6HRKqpKhS4

<!-- omit in toc -->
# TomEE start/stop scripts

- [Linux - start script](#linux---start-script)
- [Linux - stop script](#linux---stop-script)
- [Windows - start script (not maintained)](#windows---start-script-not-maintained)
- [Windows - stop script (not maintained)](#windows---stop-script-not-maintained)

## Linux - start script

```bash
#!/bin/bash
# "*****************************************************"
# "************** TomEE configuration ******************"
# "*****************************************************"

export JAVA_HOME=/usr/lib64/jvm/java-11-openjdk
export CATALINA_HOME=~/server/apache-tomee-plus-8.0.4

# optional part - useful when working with multiple TomEE instances
export DEBUG_PORT=8790
export SHUTDOWN_PORT=8005           # replace all occurrences of "8005" in con/server.xml by "${tomee.port.shutdown}"
export HTTP_PORT=8080               # replace all occurrences of "8080" in con/server.xml by "${tomee.port.http}"
export HTTPS_PORT=8443              # replace all occurrences of "8443" in con/server.xml by "${tomee.port.https}"
export AJP_PORT=8009                # replace all occurrences of "8009" in con/server.xml by "${tomee.port.ajp}"

# optional part - useful when SSL connector enabled
export HOSTNAME=localhost
export KEY_STORE_FILE=$CATALINA_HOME/certificate/serverkeystore.jks
export KEY_STORE_PASSWORD=myPwd
export KEY_STORE_ALIAS=selfsigned

# export REBEL_BASE=~/.jrebel
# export REBEL_HOME=~/java/tool/jrebel

export JAVA_OPTS=-DappNode=localhost
export JAVA_OPTS="$JAVA_OPTS -agentlib:jdwp=transport=dt_socket,address=$DEBUG_PORT,server=y,suspend=n"
export JAVA_OPTS="$JAVA_OPTS -Dtomee.port.shutdown=$SHUTDOWN_PORT -Dtomee.port.http=$HTTP_PORT -Dtomee.port.https=$HTTPS_PORT -Dtomee.port.ajp=$AJP_PORT"
export JAVA_OPTS="$JAVA_OPTS -Dtomee.cert.hostname=$HOSTNAME -Dtomee.cert.keystorefile=$KEY_STORE_FILE -Dtomee.cert.keystorepassword=$KEY_STORE_PASSWORD -Dtomee.cert.keystorealias=$KEY_STORE_ALIAS"
export JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8 -Djavax.servlet.request.encoding=UTF-8"
export JAVA_OPTS="$JAVA_OPTS -Duser.language=en -Duser.country=US -Duser.variant=US -Duser.timezone=Europe/Prague"
# Remember to change AJP connector in server.xml if your tomcat is behind an apache: <Connector ... URIEncoding="UTF-8"/>

# export JAVA_OPTS="$JAVA_OPTS -agentpath:$REBEL_HOME/lib/libjrebel64.so"

echo JAVA_HOME:$JAVA_HOME
echo PATH:$PATH
echo CATALINA_HOME:$CATALINA_HOME

# "*****************************************************"
# "************** TomEE end configuration **************"
# "*****************************************************"

# https://askubuntu.com/questions/60228/how-to-remove-all-files-from-a-directory
rm -rf $CATALINA_HOME/temp/{*,.*}
rm -rf $CATALINA_HOME/logs/{*,.*}
rm -rf $CATALINA_HOME/work/{*,.*}

sh $CATALINA_HOME/bin/catalina.sh run
```

## Linux - stop script

```bash
#!/bin/bash
echo "*****************************************************"
echo "************** TomEE configuration ******************"
echo "*****************************************************"

export JAVA_HOME=/usr/lib64/jvm/java-11-openjdk
export JRE_HOME=/usr/lib64/jvm/jre-11-openjdk
export CATALINA_HOME=~/server/apache-tomee-plus-8.0.4

echo JAVA_HOME:$JAVA_HOME
echo PATH:$PATH
echo CATALINA_HOME:$CATALINA_HOME

echo "*****************************************************"
echo "************** TomEE end configuration **************"
echo "*****************************************************"

sh $CATALINA_HOME/bin/catalina.sh stop
```

## Windows - start script (not maintained)

```cmd
echo off

set "*****************************************************"
set "************** TomEE configuration ******************"
set "*****************************************************"

set JAVA_HOME=c:\java\jdk\jdk8_64
set PATH=%JAVA_HOME%\bin;%PATH%

set DEBUG_PORT=8790

rem ENABLE or DISABLE TomEE debug
set JAVA_OPTS=%JAVA_OPTS% -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=%DEBUG_PORT%


set TOMEE_HOME=%CD%
set CATALINA_HOME=%TOMEE_HOME%
set LOG_DIR=%CD%\logs
set CATALINA_OUT=%LOG_DIR%/catalina.out

set JAVA_OPTS=%JAVA_OPTS% -Dtomee.port.shutdown=8005 -Dtomee.port.http=8080 -Dtomee.port.ajp=8009 -Dtomee.port.redirect=8443
set JAVA_OPTS=%JAVA_OPTS% -Xmx4048m -Xms2048m -verbose:gc

set "*****************************************************"
set "************ TomEE end configuration ****************"
set "*****************************************************"

rmdir %TOMEE_HOME%\temp     /s /q
mkdir %TOMEE_HOME%\temp
rmdir %TOMEE_HOME%\work     /s /q
mkdir %TOMEE_HOME%\work
rmdir %LOG_DIR%             /s /q
mkdir %LOG_DIR%

%TOMEE_HOME%/bin/catalina start
```

## Windows - stop script (not maintained)

```cmd
echo off

set TOMEE_HOME=%CD%

%TOMEE_HOME%/bin/catalina stop
```

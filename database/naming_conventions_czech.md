Existuje několik různých doporučení jak pojmenovávat databázové objekty. Nezáleží však, zda je použijeme nebo si vytvoříme vlastní, nejdůležitější je se jimi pevně řídit v celém projektu, protože nám usnadňují orientaci v databázovém systému. Tady je jedno z nich:

### Obecné ###
* jednotné číslo
* velká písmena
* oddělení slov pomocí podtržítka: _
* zkratky jen v případech, kdy je text delší než 30 znaků (nechat rezervu pro případ doplnění různých přípon)

### Tabulky ###
* název založený na jméně entity (dostatečně výstižný)
* názvy v rámci projektu jedinečné (v případě, že projekt používá více databází)
* nepoužívat obecné názvy jako soubor, tabulka apod.
* nepoužívat geografické názvy apod. (lépe nad všemi daty vytvořit view EMPLOYEE_EU_VW)

### Sloupce ###
* používat název tabulky jako prefix pouze a hlavně u primárního klíče (EMPLOYEE_ID)
* cizí klíč pojmenovat přesně podle názvu odpovídajícího sloupce primárního klíče

### Omezení ###
* TNAME_TYP_CNAME,
    * TNAME: název tabulky
    * TYP: PK | FK | UQ | CK (omezení typu check)
    * CNAME: název sloupce (sloupců), nad nímž je omezení definováno

### Indexy ###
* TNAME_TYP_CNAME
    * TNAME: název tabulky
    * TYP: UX (jedinečný index) |IX (nejedinečný index)
    * CNAME: název sloupce (sloupců), nad nímž je omezení definováno

### Pohledy ###
* NAME_VW
    * NAME: název nejdůležitější podkladové tabulky a volitelně i nějaký smysluplný text
* název by měl smysluplně popisovat účel: EMPLOYEE_EU_VW

***
**Source:**

* Andrew Oppel: Databáze bez předchozích znalostí
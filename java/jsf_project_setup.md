TODO


```
#!xml
<dependency>
    <groupId>javax</groupId>
    <artifactId>javaee-api</artifactId>
    <version>6.0</version>
    <scope>provided</scope>
</dependency>
<dependency>
    <!-- JSF API - http://mvnrepository.com/artifact/com.sun.faces/jsf-api -->
    <groupId>com.sun.faces</groupId>
    <artifactId>jsf-api</artifactId>
    <version>2.2.12</version>
    <scope>compile</scope>
</dependency>
<!-- JSF IMPL - http://mvnrepository.com/artifact/com.sun.faces/jsf-impl -->
<dependency>
    <groupId>com.sun.faces</groupId>
    <artifactId>jsf-impl</artifactId>
    <version>2.2.12</version>
    <scope>compile</scope>
</dependency>
<!-- Needed for Jetty, Tomcat & TomEE -->
<dependency>
    <groupId>javax.el</groupId>
    <artifactId>el-api</artifactId>
    <version>2.2</version>
    <scope>compile</scope>
</dependency>
<dependency>
    <groupId>org.glassfish.web</groupId>
    <artifactId>el-impl</artifactId>
    <version>2.2</version>
    <scope>compile</scope>
</dependency>
<!-- PrimeFaces - Commercial versions on http://primefaces.org/downloads -->
<dependency>
    <groupId>org.primefaces</groupId>
    <artifactId>primefaces</artifactId>
    <version>5.3</version>
</dependency>
<dependency>
    <groupId>org.primefaces.themes</groupId>
    <artifactId>bootstrap</artifactId>
    <version>1.0.10</version>
</dependency>
<!-- PF Extensions - http://mvnrepository.com/artifact/org.primefaces.extensions/primefaces-extensions -->
<dependency>
    <groupId>org.primefaces.extensions</groupId>
    <artifactId>primefaces-extensions</artifactId>
    <version>4.0.0</version>
</dependency>
<!-- OmniFaces - http://mvnrepository.com/artifact/org.omnifaces/omnifaces -->
<dependency>
    <groupId>org.omnifaces</groupId>
    <artifactId>omnifaces</artifactId>
    <version>2.2</version>
</dependency>
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-lang3</artifactId>
    <version>3.4</version>
</dependency>
<dependency>
    <groupId>joda-time</groupId>
    <artifactId>joda-time</artifactId>
    <version>2.9.1</version>
</dependency>
<!-- Test -->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>
<dependency>
    <groupId>org.mockito</groupId>
    <artifactId>mockito-all</artifactId>
    <version>2.0.2-beta</version>
    <scope>test</scope>
</dependency>
```

web.xml

```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://java.sun.com/xml/ns/javaee"
         xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
         id="WebApp_ID"
         version="3.0">

    <display-name>PrimeFaces APP</display-name>

    <!-- Welcome page -->
    <welcome-file-list>
        <welcome-file>index.xhtml</welcome-file>
    </welcome-file-list>

    <session-config>
        <session-timeout>10</session-timeout>
    </session-config>

    <!-- Time in seconds that facelets should be checked for changes since last request. A value of -1 disables refresh checking. -->
    <context-param>
        <param-name>javax.faces.FACELETS_REFRESH_PERIOD</param-name>
        <param-value>2</param-value>
    </context-param>

    <!--
    Set the project stage to "Development", "UnitTest", "SystemTest", or "Production".
    An optional parameter that makes troubleshooting errors much easier.
    Change to "Production" when you are ready to deploy!
    -->
    <context-param>
        <param-name>javax.faces.PROJECT_STAGE</param-name>
        <param-value>Development</param-value>
    </context-param>

    <context-param>
        <param-name>primefaces.SUBMIT</param-name>
        <param-value>partial</param-value>
    </context-param>

    <context-param>
        <param-name>javax.faces.STATE_SAVING_METHOD</param-name>
        <param-value>client</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL</param-name>
        <param-value>true</param-value>
    </context-param>
    <context-param>
        <param-name>primefaces.FONT_AWESOME</param-name>
        <param-value>true</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.FACELETS_SKIP_COMMENTS</param-name>
        <param-value>true</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.VALIDATE_EMPTY_FIELDS</param-name>
        <param-value>true</param-value>
    </context-param>

    <!-- JSF Servlet -->
    <servlet>
        <servlet-name>Faces Servlet</servlet-name>
        <servlet-class>javax.faces.webapp.FacesServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>Faces Servlet</servlet-name>
        <url-pattern>*.xhtml</url-pattern>
    </servlet-mapping>

    <context-param>
        <param-name>primefaces.THEME</param-name>
        <param-value>bootstrap</param-value>
    </context-param>

    <!-- Explicitly mentioned here because of Jetty and GlassFish 3
    <listener>
        <listener-class>com.sun.faces.config.ConfigureListener</listener-class>
    </listener>
    -->

    <mime-mapping>
        <extension>less</extension>
        <mime-type>stylesheet/less</mime-type>
    </mime-mapping>
    <mime-mapping>
        <extension>eot</extension>
        <!--<mime-type>application/vnd.ms-fontobject</mime-type>-->
        <mime-type>application/octet-stream</mime-type>
    </mime-mapping>
    <mime-mapping>
        <extension>woff</extension>
        <!--<mime-type>application/font-woff</mime-type>-->
        <mime-type>application/octet-stream</mime-type>
    </mime-mapping>
    <mime-mapping>
        <extension>woff2</extension>
        <!--<mime-type>application/x-font-woff2</mime-type>-->
        <mime-type>application/octet-stream</mime-type>
    </mime-mapping>
    <mime-mapping>
        <extension>ttf</extension>
        <!--<mime-type>application/x-font-ttf</mime-type>-->
        <mime-type>application/octet-stream</mime-type>
    </mime-mapping>
    <mime-mapping>
        <extension>otf</extension>
        <!--<mime-type>application/x-font-opentype</mime-type>-->
        <mime-type>application/octet-stream</mime-type>
    </mime-mapping>
    <mime-mapping>
        <extension>svg</extension>
        <mime-type>image/svg+xml</mime-type>
    </mime-mapping>

</web-app>
```
<!-- omit in toc -->
# JSF Getting Started

- [Documentation & links](#documentation--links)
  - [Basic](#basic)
  - [Blogs](#blogs)
- [Tips&Tricks](#tipstricks)

## Documentation & links

### Basic

- Book: The Definitive Guide to JSF in Java EE 8 (2018) by Bauke Scholtz (BalusC)
  - <https://www.apress.com/gp/book/9781484233863>

- Top Java/JSF topics by BalusC
  - <https://jsf.zeef.com/bauke.scholtz>
  
- JSF
  - as
  - df

- Apache MyFaces
  - <http://myfaces.apache.org/>
  - IssueTracker <https://issues.apache.org/jira/browse/MYFACES-4354?jql=project%20%3D%20MYFACES>
  - User Guide <https://cwiki.apache.org/confluence/display/MYFACES/MyFaces+Core+User+Guide>
  - Web Context Parameters <https://myfaces.apache.org/core22/myfaces-impl/webconfig.html>

- PrimeFaces
  - Showcase <https://www.primefaces.org/showcase/>
  - Documentation <https://primefaces.github.io/primefaces/9_0/#/>
  - JavaScript API Docs <https://primefaces.github.io/primefaces/jsdocs/index.html>
  - Blog <https://www.primefaces.org/blog/>)
  - GitHub/Issue tracker <https://github.com/primefaces/primefaces>

- PrimeFaces Extensions
  - Showcase <https://www.primefaces.org/showcase-ext/>
  - GitHub/Issue tracker <https://github.com/primefaces-extensions/primefaces-extensions.github.com>
  
- OmniFaces

### Blogs

- BalusC <https://balusc.omnifaces.org/>

- <http://tandraschko.blogspot.com/>
  - Increase your JSF application performance - Version 2017
  - JSF benchmark 2019
  - Way to MyFaces 2.3-next
  - etc.

## Tips&Tricks


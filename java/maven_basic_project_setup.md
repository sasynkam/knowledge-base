# Maven

## Setup

* Set M2_HOME & path in system [[Maven in 5 minutest](http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)]
* Or edit `.bashrc` in user home directory

 ```bash
 export M2_HOME=~/Programs/apache-maven-3.6.1/
 export M2=$M2_HOME/bin
 export PATH=$M2:$PATH
 ```

* Reload `.bashrc` by

```bash
source ~/.bashrc
```

* Verify

```bash
mvn -v
```

* You can set path of your local repository by `<localRepository>` M2_HOME\conf\settings.xml

## Import library to the repository

```bash
mvn install:install-file -Dfile=primefaces-7.0.4.jar -DgroupId=org.primefaces -DartifactId=primefaces -Dversion=7.0.4 -Dpackaging=jar
```

## Maven's project structure

![base_maven_project-structure.png](https://bitbucket.org/repo/xzykkA/images/2115591420-base_maven_project-structure.png)

## Basic Maven pom.xml configuration

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
		 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			   xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>org.bitbucket.sasynkam</groupId>
	<artifactId>web-app</artifactId>
	<version>0.1-SNAPSHOT</version>
	<packaging>war</packaging>
	
	<name>WebApp</name>
	<url>https://sasynkam.bitbucket.org</url>
	<description>Some web application.</description>
	
	<properties>
	</properties>
	
	<build>
		<plugins>
			<!-- Set Java version, default is 1.3 -->
			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
				<version>3.3</version>
			</plugin>
			<!-- Newer maven plugin for working with Java 7+ -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>2.6</version>
			</plugin>
		</plugins>
	</build>
	
	<dependencies>
	</dependencies>
	
	<repositories>
	</repositories>
	
</project>
```

* Check your IDE if source and test folders are properly mapped

![idea_maven_folders.png](https://bitbucket.org/repo/xzykkA/images/857369507-idea_maven_folders.png)
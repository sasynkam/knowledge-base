*There is always need to set specific parameters for development and production environment in web.xml. We can use maven to do this automatically based on profile.*

* Put content into web.xml surrounded by comment characters
```
#!xml
<!-- ${developmentProfile.start}
${developmentProfile.end} -->

<!-- ${productionProfile.start}
${productionProfile.end} -->
```
* Add maven-war-plugin and configure it to process web.xml:

```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-war-plugin</artifactId>
    <version>2.6</version>
    <configuration>
        <webResources>
            <resource>
                <filtering>true</filtering>
                <directory>src/main/webapp</directory>
                <includes>
                    <include>**/web.xml</include>
                </includes>
            </resource>
        </webResources>
    </configuration>
</plugin>
```

* Add profiles to the maven:


```
#!xml
<profiles>
    <profile>
        <id>development</id>
	    <properties>
            <developmentProfile.start>--&gt;</developmentProfile.start>
            <developmentProfile.end>&lt;!--</developmentProfile.end>
       </properties>
    </profile>
    <profile>
        <id>production</id>
        <properties>
            <productionProfile.start>--&gt;</productionProfile.start>
            <productionProfile.end>&lt;!--</productionProfile.end>
        </properties>
    </profile>
</profiles>
```

* As result maven plugin 'uncomment' given part in web.xml...

```
#!xml
<!-- -->
<context-param>
    <param-name>javax.faces.PROJECT_STAGE</param-name>
    <param-value>Development</param-value>
</context-param>
<!-- -->
<!-- ${productionProfile.start}
<context-param>
    <param-name>javax.faces.PROJECT_STAGE</param-name>
    <param-value>Development</param-value>
</context-param>
${productionProfile.end} -->
```
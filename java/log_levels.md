# Log levels

## TRACE

Most detailed information. Expect these to be written to logs only.

Nejnižší úroveň, obvykle vypisujeme pouze do souboru. Vhodné pro různé check-pointy (metoda zavolána, různé počítadla pro ladící účely). Tato úroveň může obsahovat mnoho zpráv, často je programátory nevyužívána a je psáno zbytečně mnoho informací do úrovně DEBUG.

## DEBUG

Detailed information on the flow through the system. Expect these to be written to logs only.

Ladící hlášky, které pomáhají zákazníkovi identifikovat prvotní problém, případně vytvořit report pro dodavatele, aby mohl chybu opravit. V DEBUG by nemělo být příliš hlášek, ale zároveň by tato úroveň měla poskytovat dostatek informací k tomu, aby mohlo být zpětně zjištěno, v čem je problém.

## INFO

Interesting runtime events (startup/shutdown). Expect these to be immediately visible on a console, so be conservative and keep to a minimum.

Hlášky INFO obvykle nejsou vypisovány ve standardní instalaci, ale zákazník si může zvýšit vypisování z WARN na INFO a měl by vidět o něco více informací. Hlášky INFO jsou vhodné zejména pro různé informační zprávy typu něco se startuje, server znovu načetl konfiguraci nebo že aplikace načítá soubor z disku.

## WARN

Use of deprecated APIs, poor use of API, 'almost' errors, other runtime situations that are undesirable or unexpected, but not necessarily "wrong". Expect these to be immediately visible on a status console.

Tato úroveň by měla být zapnuta v implicitní instalaci softwaru, program informuje o události, která není obvyklá. Příklad – potřebná konfigurační hodnota nebyla nalezena v konfiguraci, program použije   defaultní hodnotu. Je vždy dobré se zamyslet, jestli daná je daná hláška chybou (ERROR), varováním (WARN) nebo dokonce pouze informativního charakteru (INFO).

## ERROR

Other runtime errors or unexpected conditions. Expect these to be immediately visible on a status console.

V aplikaci došlo k chybovému stavu, který však nenaruší běh programu. Typicky například nemohl být zpracován požadavek na serveru kvůli špatné konfiguraci nebo třeba chyba vstupních dat – program neprovede svoji činnost, ale čeká na další (validní) vstup.

## FATAL

Severe errors that cause premature termination. Expect these to be immediately visible on a status console.

Došlo k vyjímečnému stavu a program ukončuje svoji činnost. Například soubor nebyl vůbec nalezen, chyba v programu nebo došla paměť.
